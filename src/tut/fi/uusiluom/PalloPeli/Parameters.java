// Juho Uusi-Luomalahti 	10.5.2014
// Luokka sis�lt�� parametrit, joilla pelin ominaisuuksia voi s��t��
// Hedelmien paramtetrit l�ytyv�t omista luokistaan.

package tut.fi.uusiluom.PalloPeli;

public class Parameters {
	
	static public final int LIMIT = 7;					// pelialueen rajat (dp)
	static public final int T_INCR = 1;				//laskennassa k�ytetty aikainkrementti (ms)
	static public final double BOUNCE_FACTOR_DEFAULT = 0.55;	//sein�st� kimpoamiskertoimen oletusarvo
	static public final double ENEMY_SPEED = 0.10;	//vihollisen nopeus
	static public final double ENEMY_BRAKE = 0.998;	//vihollisen hidastuvuus (isompi = v�hemm�n jarrua)
	static public final float ENEMY_OFFSET = 15;		//vihollisen kosketusoffset
	static public final double HIDASTUS_DEFAULT = 0.245;	//pelaajan hidastuksen oletusarvo, isompi -> nopeampi
	static public final int SHAKEWALL_TIME = 5;		// sein�n v�r�htelyaika pallon osuessa siihen (kierroksia onDraw-metodissa)
	static public final double SHAKEWALL_ATTENUATION = -0.9;		// sein�n v�r�htelyn vaimennus (huom. abs <1)
	static public final double SHAKEWALL_INIT_DISLOC = 0.04;	//sein�n ensimm�isen v�r�hdyksen amplitudi (dp)
}
