package tut.fi.uusiluom.PalloPeli;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
// Juho Uusi-Luomalahti 27.4.2014

//Vesimeloni
//10 pistett�

public class Vesimeloni extends Fruit  {
	private final int POINTS = 10;	//yhdest� sienest� saatavat pisteet
	private final int FRUIT_LIFE_TIME = 300;		//hedelm�n elinaika (kierroksia onDraw -metodissa)
	private final float FRUIT_OFFSET = 38;		// hedelm�n kosketusoffset (dp)
	
	private int fruitW, fruitH;
	private Bitmap fruit;
	private int fruitAgeCounter;	//elinaikalaskuri hedelm�lle
	
	public Vesimeloni(Context context, int screenWidth, int screenHeight){		//konstruktori
		super(context, screenWidth, screenHeight);
		fruit = BitmapFactory.decodeResource(context.getResources(),R.drawable.watermelon); 	//hedelm�n bitmap
        this.fruitW = fruit.getWidth();
        this.fruitH = fruit.getHeight();
	}
	
	@Override
	public boolean GrabFruit(float draw_CoordX, float draw_CoordY){
		return super.DoesTouchFruitMelon(FRUIT_OFFSET, draw_CoordX, draw_CoordY, fruitW, fruitH);
	}

	@Override
	public boolean IsFruitAlive(){
		fruitAgeCounter++;
		if(fruitAgeCounter > FRUIT_LIFE_TIME){
			return false;
		}
		return true;
	}
	
	@Override
	public int getPoints(){
		return POINTS;
	}
	
	@Override
	public Bitmap getBitmap(){
		return fruit;
	}
}
