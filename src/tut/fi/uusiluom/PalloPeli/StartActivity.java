// Juho Uusi-Luomalahti 27.4.2014

// Pallopeli
// Ker�ile hedelmi� ja pakoile vihollista!


package tut.fi.uusiluom.PalloPeli;

import java.util.Random;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

public class StartActivity extends Activity implements SensorEventListener {

	private SensorManager sensorManager;
	public static float xAcc, yAcc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(new DrawCanvas(this));
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);	//pidet��n aktiivisena
	    sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
	}
	
	  // event trigger
	  @Override
	  public void onSensorChanged(SensorEvent event) {
	    if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
		    float[] values = event.values;
		    // kiihtyvyysarvot
		    yAcc = values[1];
		    xAcc = values[0];
	    }
	  }

	  @Override
	  public void onAccuracyChanged(Sensor sensor, int accuracy) {
	  }
	  
	  @Override
	  protected void onResume() {
	    super.onResume();
	    // accelerometer listener
	    sensorManager.registerListener(this,
	        sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
	  }

	  @Override
	  protected void onPause() {
	    super.onPause();
	    sensorManager.unregisterListener(this);
	  }
	  
	  @Override
	  protected void onStop() {
		  super.onStop();
		  finish();
	  }
}


class DrawCanvas extends View {
	
	static private double bounceFactor;		// sein�st� kimpoamiskertoimen muuttuja
	static private double hidastus;				// hidastusmuuttuja
	
    private double pos_Xcur, pos_Ycur;			//nykyisen ajanhetken paikka
    private double pos_Xprev, pos_Yprev;		//edellisen ajanhetken paikka
    private double veloc_Xcur, veloc_Ycur;		//nykyisen ajanhetken nopeudet
    private double veloc_Xprev, veloc_Yprev;	//edellisen ajanhetken nopeudet
    private float draw_CoordX, draw_CoordY;		//pallon piirtokoordinaatit
    
    private int enemy_posX, enemy_posY;					//vihollisen sijainnit
    private int enemy_drawCoord_X, enemy_drawCoord_Y;	//vihollisen piirtokoordinaatit(keskipiste)
    private double enemy_velocX, enemy_velocY;			//vihollisen nopeudet
    private double target_X, target_Y;					//kohteen(pelaajan) ja vihollisen v�liset koordinaattiet�isyydet
    private double targetDist;							//euklidinen kohtisuora et�isyys vihollisen ja pelaajan v�lill�
    private static boolean enemyFlag;					//kertoo onko vihollinen saanut kiinni
    
    private int screenWidth, screenHeight;		//n�yt�n koko
    private int ballW, ballH;					//pelaajan pallon koko
    private int enemyW, enemyH;					//vihollisen pallon koko
    private boolean shakeWallFlagX, shakeWallFlagY;
    private int shakeWallCounterX, shakeWallCounterY;
    private int wallDislocationX, wallDislocationY;
    
    private Bitmap ball, bgr, enemy;		//bitmapit
    private Paint paint;						//kohteiden piirtoa varten (numerot, reunat)
    private Random rndGen;
    private static int points;					//hedelm�pisteet
    
    private int fruitEat, fail, hitWall, hitWallEnemy;	//��net
    private SoundPool sp;								//��ni� varten
    
    private Fruit[] fruits;							//taulukko eri hedelmille
    private boolean noFruitsFlag;				//kertoo onko kaikki hedelm�t sy�ty

    // konstruktori
    public DrawCanvas(Context context) {
        super(context);
        
        bgr = BitmapFactory.decodeResource(getResources(),R.drawable.background); 	//taustakuva
    	points = 0;
    	targetDist = 1;
        paint = new Paint();
    	rndGen = new Random(); 
        noFruitsFlag = true;
        fruits = new Fruit[5];
        hidastus = Parameters.HIDASTUS_DEFAULT;
        bounceFactor = Parameters.BOUNCE_FACTOR_DEFAULT;
        shakeWallFlagX = false; shakeWallFlagY = false;
        shakeWallCounterX = 0; shakeWallCounterY = 0;
        
        // ��ni�
        sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        fruitEat = sp.load(context, R.raw.fruiteat, 1);
        fail = sp.load(context, R.raw.failbuzzer, 1);
        hitWall = sp.load(context, R.raw.hitwall, 1);
        
        // vihollisen m��rityksi�
        hitWallEnemy = sp.load(context, R.raw.hitwallenemy, 1);	//��ni
        enemy = BitmapFactory.decodeResource(getResources(),R.drawable.enemy2); 	//vihollisen bitmap
        enemyW = enemy.getWidth();
        enemyH = enemy.getHeight();
    	enemyFlag = false;
    	enemy_posX = 0; enemy_posY = 0;
        
    	// pelaajan pallon m��rityksi�
        ball = BitmapFactory.decodeResource(getResources(),R.drawable.hahmo2); 		//pallo
        ballW = ball.getWidth();
        ballH = ball.getHeight();
        veloc_Xcur = 0; veloc_Ycur = 0;
        veloc_Xprev = 0; veloc_Yprev = 0;
    }

    @Override
    public void onSizeChanged (int width, int height, int oldWidth, int oldhHeight) {
        super.onSizeChanged(width, height, oldWidth, oldhHeight);
        screenWidth = width;
        screenHeight = height;
        bgr = Bitmap.createScaledBitmap(bgr, width, height, true); 	//sovitetaan taustakuva n�ytt��n
        pos_Xprev = (int) (screenWidth /2); 		// keskitet��n pallo n�yt�n keskelle
        pos_Yprev = (int) (screenHeight /2); 
        pos_Xcur = 0; pos_Ycur = 0;
        enemy_posX = enemyW;
        enemy_posY = enemyH;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // pieni viive, jotta eri tehoiset laitteet k�ytt�ytyisiv�t suunilleen samalla tavalla
        try {
            Thread.sleep(Parameters.T_INCR);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        
        // piirret��n rajat ja taustakuva
        canvas.drawBitmap(bgr, 0, 0, null);
        
        //tutkitaan mik� hedelm� on sy�ty ja muutetaan sen perusteella reunuksen v�ri�
        if( (Sitruuna.getLemonEatenFlag() == false) && (Sieni.getShroomEatenFlag() == false) &&
        		(Porkkana.getCarrotEatenFlag() == false) ){
            paint.setStrokeWidth(Dp2Pixel(4));
		    paint.setColor(0xffBFBFBF);		// (kaksi ekaa numeroa m��ritt�� l�pin�kyvyyden)
        }
        else if(Sitruuna.getLemonEatenFlag()){
        	paint.setStrokeWidth(Dp2Pixel(5.5f));
		    paint.setColor(0xffFFFF00);	
        }
        else if(Sieni.getShroomEatenFlag()){
        	paint.setStrokeWidth(Dp2Pixel(5.5f));
		    paint.setColor(0xffD900E0);	
        }
        else if(Porkkana.getCarrotEatenFlag()){
        	paint.setStrokeWidth(Dp2Pixel(5.5f));
		    paint.setColor(0xffF57E00);	
        }
        
        // heilautetaan sein�m�� jos vihollinen/pelaaja osuu sein��n
    	if(shakeWallFlagX){
    		ShakeWallX();
    	}
    	if(shakeWallFlagY){
    		ShakeWallY();
    	}
    	
    	
    	
        //piirret��n sein�m�t
        paint.setStyle(Paint.Style.STROKE);
	    canvas.drawRect(Dp2Pixel(Parameters.LIMIT + wallDislocationX), Dp2Pixel(Parameters.LIMIT + wallDislocationY), 
	    		screenWidth-Dp2Pixel(Parameters.LIMIT - wallDislocationX), screenHeight-Dp2Pixel(Parameters.LIMIT - wallDislocationY), paint );
        
        // piirret��n pistetilanne oikeaan yl�kulmaan
        paint.setColor(Color.RED); 
        paint.setTextSize(Dp2Pixel(50)); 
        canvas.drawText(Integer.toString(points), screenWidth-Dp2Pixel(110), 
        		Dp2Pixel(65), paint); 
        
        // piirret��n hedelm�t ja tutkitaan osutaanko niihin
        if(noFruitsFlag == false){	//jos hedelmi� on sy�m�tt� (ei k�yd� turhaan taulukkoa l�pi)
        	noFruitsFlag = true;
        	for (int i = 0; i < fruits.length; i++) {
	        	
        		// tutkitaan onko hedelm� hengiss�. jos elinaika on loppu niin nullitetaan hedelm�
        		if( (fruits[i] != null) && (fruits[i].IsFruitAlive() == false) ){
        			fruits[i] = null;
        		}
        		
	        	if( fruits[i] != null){
	        		// jos taulukon paikassa i on hedelm� niin piirret��n se
	        		canvas.drawBitmap(fruits[i].getBitmap(), fruits[i].getCoordX(), fruits[i].getCoordY(), null);
	        		noFruitsFlag = false;
	        		
	        		// jos osutaan hedelm��n
		        	if( fruits[i].GrabFruit(draw_CoordX, draw_CoordY) == true ){
		        		sp.play(fruitEat, 1, 1, 0, 0, 1);		//��ni
		            	points = points + fruits[i].getPoints();
		            	fruits[i] = null;
		            }
	        	}
			}
        }
        
        // tutkitaan vaikuttavatko sy�dyt hedelm�t pelaajaan
        Sieni.IsShroomAffecting();
        Sitruuna.IsLemonAffecting();
        Porkkana.IsCarrotAffecting();
        
        GenFruits();		//generoidaan uudet hedelm�t
        EnemyPosition();		//lasketaan vihollisen sijainti
        EnemyOutLimits();		//onko vihollinen rajojen yli?
        CalcPosAndVeloc();		//lasketaan pelaajan pallon nopeudet ja sijainnit
        IsOutLimits();			// onko pallo rajojen yli?
        EnemyCatch();			//onko vihollinen saanut kiinni?
        
        // piirret��n pallo, k�ytet��n keskipistekoordinaattia
        canvas.drawBitmap(ball, draw_CoordX, draw_CoordY, null);
        
        // piirret��n vihollinen, k�ytet��n keskipistekoordinaattia
        canvas.drawBitmap(enemy, enemy_drawCoord_X, enemy_drawCoord_Y, paint);
        
        // pelaajan pallon paikanlaskentaa varten sijoitetaan muuttujat
        pos_Xprev = pos_Xcur;
        pos_Yprev = pos_Ycur;
        veloc_Xprev = veloc_Xcur;
        veloc_Yprev = veloc_Ycur;
        
        invalidate();	// next frame
    }
    
    
    
    //************************* Generoidaan uudet hedelm�t *****************************
   	// generoidaan uusia hedelmi� jos kaikki on sy�ty
    public void GenFruits(){
        if(noFruitsFlag == true){
        	int probFruitQty = rndGen.nextInt(100) + 1;		//todenn�k�isyys hedelmien m��r�lle
        	
        	// yksi hedelm�
        	if(probFruitQty <= 70){
        		fruits[0] = pickFruitSpecie();
        		noFruitsFlag = false;
        	}
        	//kaksi hedelm��
        	else if ( (probFruitQty >= 71) && (probFruitQty <= 85) ){
        		fruits[0] = pickFruitSpecie();
        		fruits[1] = pickFruitSpecie();
        		noFruitsFlag = false;
        	}
        	//kolme hedelm��
        	else if ( (probFruitQty >= 86) && (probFruitQty <= 92) ){
        		fruits[0] = pickFruitSpecie();
        		fruits[1] = pickFruitSpecie();
        		fruits[2] = pickFruitSpecie();
        		noFruitsFlag = false;
        	}
        	//nelj� hedelm��
        	else if ( (probFruitQty >= 93 ) && (probFruitQty <= 96) ){
        		fruits[0] = pickFruitSpecie();
        		fruits[1] = pickFruitSpecie();
        		fruits[2] = pickFruitSpecie();
        		fruits[3] = pickFruitSpecie();
        		noFruitsFlag = false;
        	}
        	//viisi hedelm��
        	else if ( (probFruitQty >= 97 ) && (probFruitQty <= 100) ){
        		fruits[0] = pickFruitSpecie();
        		fruits[1] = pickFruitSpecie();
        		fruits[2] = pickFruitSpecie();
        		fruits[3] = pickFruitSpecie();
        		fruits[4] = pickFruitSpecie();
        		noFruitsFlag = false;
        	}
        }
    }
    
    // ******************** m��ritet��n hedelm�n lajike **************************
    public Fruit pickFruitSpecie(){
       	int probFruitSpecie = rndGen.nextInt(100) + 1;	//todenn�k�isyys hedelm�n lajikkeelle
    	
       	// mansikka
    	if(probFruitSpecie <= 88){
    		return new Mansikka(getContext(), screenWidth, screenHeight);
    	}
    	// sitruuna
    	else if ( (probFruitSpecie >= 89) && (probFruitSpecie <= 91) ){
    		return new Sitruuna(getContext(), screenWidth, screenHeight);
    	}
    	// porkkana
    	else if ( (probFruitSpecie >= 92) && (probFruitSpecie <= 95) ){
    		return new Porkkana(getContext(), screenWidth, screenHeight);
    	}
    	// sieni
    	else if ( (probFruitSpecie >= 96 ) && (probFruitSpecie <= 98) ){
    		return new Sieni(getContext(), screenWidth, screenHeight);
    	}
    	// vesimeloni
    	else if ( (probFruitSpecie >= 99 ) && (probFruitSpecie <= 100) ){
    		return new Vesimeloni(getContext(), screenWidth, screenHeight);
    	}
    	
		return null;	//ehtorakenteessa on kaikki tilanteet, mutta eclipse vaatii silti returnin
    }
    
    
    //*********************** Lasketaan vihollisen sijainti ***************************
    public void EnemyPosition(){
    	// lasketaan miss� suunnassa kohde on
    	targetDist = Math.sqrt( Math.pow((pos_Xcur - enemy_posX), 2) + Math.pow((pos_Ycur - enemy_posY), 2) );
    	target_X = (pos_Xcur - enemy_posX) / targetDist;
    	target_Y = (pos_Ycur - enemy_posY) / targetDist;
    	
    	// lasketaan x- ja y-suuntaiset nopeudet viholliselle
    	enemy_velocX = enemy_velocX*Parameters.ENEMY_BRAKE + target_X*Parameters.ENEMY_SPEED;
    	enemy_velocY = enemy_velocY*Parameters.ENEMY_BRAKE + target_Y*Parameters.ENEMY_SPEED;
    	
    	// lasketaan sijainnit viholliselle
    	enemy_posX = (enemy_posX + (int)enemy_velocX); 
    	enemy_posY = (enemy_posY + (int)enemy_velocY);
    	
        //lasketaan vihollisen piirtokoordinaatit, k�ytet��n keskipistekoordinaattia
    	enemy_drawCoord_X = enemy_posX - (enemyW/2);
    	enemy_drawCoord_Y = enemy_posY - (enemyH/2);
    }
    
    
    // ************* Tutkitaan onko vihollinen rajojen yli *****************
    public void EnemyOutLimits(){
    	if( enemy_posX < Dp2Pixel(Parameters.LIMIT)+(enemyW/2)){
    		sp.play(hitWallEnemy, 1, 1, 0, 0, 1);
    		enemy_posX = (int)(Dp2Pixel(Parameters.LIMIT)+(enemyW/2)+5);
    		enemy_velocX = enemy_velocX*(-bounceFactor);
        }
        else if( enemy_posX > screenWidth-Dp2Pixel(Parameters.LIMIT)-(enemyW/2) ){
        	sp.play(hitWallEnemy, 1, 1, 0, 0, 1);
        	enemy_posX = (int)(screenWidth-Dp2Pixel(Parameters.LIMIT)-(enemyW/2)-5);
        	enemy_velocX = enemy_velocX*(-bounceFactor);
        }
        if( enemy_posY < Dp2Pixel(Parameters.LIMIT)+(enemyH/2) ){
        	sp.play(hitWallEnemy, 1, 1, 0, 0, 1);
        	enemy_posY = (int)(Dp2Pixel(Parameters.LIMIT)+(enemyH/2)+5);
        	enemy_velocY = enemy_velocY*(-bounceFactor);
        }
        else if( enemy_posY > screenHeight-Dp2Pixel(Parameters.LIMIT)-(enemyH/2) ){
        	sp.play(hitWallEnemy, 1, 1, 0, 0, 1);
        	enemy_posY = (int)(screenHeight-Dp2Pixel(Parameters.LIMIT)-(enemyH/2)-5);
        	enemy_velocY = enemy_velocY*(-bounceFactor);
        }
    }
    
    // ************* Tutkitaan onko vihollinen saanut kiinni *****************
    public void EnemyCatch(){
    	if( (enemyFlag == false) && pos_Xcur > (enemy_posX-(enemyW/2)-Dp2Pixel(Parameters.ENEMY_OFFSET) ) && 
    			( pos_Xcur < (enemy_posX+(enemyW/2)+Dp2Pixel(Parameters.ENEMY_OFFSET)) ) && 
    			( pos_Ycur > (enemy_posY-(enemyH/2)-Dp2Pixel(Parameters.ENEMY_OFFSET)) ) &&
    			( pos_Ycur < (enemy_posY+(enemyH/2)+Dp2Pixel(Parameters.ENEMY_OFFSET)) ) ){
    		enemyFlag = true;
    		
    		sp.play(fail, 1, 1, 0, 0, 1);
    		
    		//k�ynnistett��n HighscoreActivity
    		Intent intent = new Intent(getContext(), HighscoreActivity.class);
    		getContext().startActivity(intent);
    		
    		//viive 500 ms
    		try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

    	}
    }
    
    
    // *************** Tutkitaan onko pallo rajojen yli (osuuko sein��n) *******************
    public void IsOutLimits(){
    	if( pos_Xcur < Dp2Pixel(Parameters.LIMIT)+(ballW/2)){
    		sp.play(hitWall, 1, 1, 0, 0, 1);
    		pos_Xcur = Dp2Pixel(Parameters.LIMIT)+(ballW/2);
    		veloc_Xcur = veloc_Xcur*(-bounceFactor);
        	shakeWallFlagX = true;
        }
        else if( pos_Xcur > screenWidth-Dp2Pixel(Parameters.LIMIT)-(ballW/2) ){
        	sp.play(hitWall, 1, 1, 0, 0, 1);
        	pos_Xcur = screenWidth-Dp2Pixel(Parameters.LIMIT)-(ballW/2);
        	veloc_Xcur = veloc_Xcur*(-bounceFactor);
        	shakeWallFlagX = true;
        }
        if( pos_Ycur < Dp2Pixel(Parameters.LIMIT)+(ballH/2) ){
        	sp.play(hitWall, 1, 1, 0, 0, 1);
        	pos_Ycur = Dp2Pixel(Parameters.LIMIT)+(ballH/2);
        	veloc_Ycur = veloc_Ycur*(-bounceFactor);
        	shakeWallFlagY = true;
        }
        else if( pos_Ycur > screenHeight-Dp2Pixel(Parameters.LIMIT)-(ballH/2) ){
        	sp.play(hitWall, 1, 1, 0, 0, 1);
        	pos_Ycur = screenHeight-Dp2Pixel(Parameters.LIMIT)-(ballH/2);
        	veloc_Ycur = veloc_Ycur*(-bounceFactor);
        	shakeWallFlagY = true;
        }
    }
    
    // ************** lasketaan pallon nopeudet ja sijainnit ******************
    public void CalcPosAndVeloc(){
    	// (kiihtyvyyden 1.derivaatta nopeus, 2.derivaatta sijainti)
        // pallon nopeus
        veloc_Xcur = veloc_Xprev + ((-1)*StartActivity.xAcc*hidastus);
        veloc_Ycur = veloc_Yprev + (StartActivity.yAcc*hidastus);
        // pallon sijainti
        pos_Xcur = pos_Xprev + (veloc_Xprev*hidastus) + ((veloc_Xcur-veloc_Xprev)*hidastus*(0.5));
        pos_Ycur = pos_Yprev + (veloc_Yprev*hidastus) + ((veloc_Ycur-veloc_Yprev)*hidastus*(0.5));
 
        draw_CoordX = (float)pos_Xcur-(ballW/2);
        draw_CoordY = (float)pos_Ycur-(ballH/2);
    }
    
    
    // **************** muunnetaan dp -> pixel ******************
    public float Dp2Pixel(float dp){
	  DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
	  float px = dp * (metrics.densityDpi/160f);
	  return px;
	}
    
    
    //************************* sein�m�n v�r�hdys pelaajan/vihollisen osuessa sein��n ******************
    // x-suunta
    private void ShakeWallX(){
    	shakeWallCounterX++;
    	int tempCounter = shakeWallCounterX / 2;
    	if( (tempCounter) < Parameters.SHAKEWALL_TIME ){
	    	wallDislocationX = (int) (veloc_Xcur * Parameters.SHAKEWALL_INIT_DISLOC *
	    			Math.pow(Parameters.SHAKEWALL_ATTENUATION, tempCounter*1.0));
    	}
    	else{
    		shakeWallCounterX = 0;
    		wallDislocationX = 0;
    		shakeWallFlagX = false;
    	}
    }
    
    //************************* sein�m�n v�r�hdys pelaajan/vihollisen osuessa sein��n ******************
    // y-suunta
    private void ShakeWallY(){
    	shakeWallCounterY++;
    	int tempCounter = shakeWallCounterY / 2;
    	if(tempCounter < Parameters.SHAKEWALL_TIME){
    		wallDislocationY = (int) (veloc_Ycur * Parameters.SHAKEWALL_INIT_DISLOC *
    				Math.pow(Parameters.SHAKEWALL_ATTENUATION, tempCounter*1.0));
    	}
    	else{
    		shakeWallCounterY = 0;
    		wallDislocationY = 0;
    		shakeWallFlagY = false;
    	}
    }
    
    
    // ***************** setterit ja getterit *********************
    public static void setEnemyFlag(boolean state){
    	enemyFlag = state;
    }
    
    public static void setPoints(int points2){
    	points = points2;
    }
    
    public static int getPoints(){
    	return points;
    }
    
    public static void setHidastus(double amount){
    	hidastus = amount;
    }
    
    public static void setHidastus2Default(){
    	hidastus = Parameters.HIDASTUS_DEFAULT;
    }
    
    public static void setBounceFactor(double amount){
    	bounceFactor = amount;
    }

    public static void setBounceFactor2Default(){
    	bounceFactor = Parameters.BOUNCE_FACTOR_DEFAULT;
    }
}