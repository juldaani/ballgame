// Juho Uusi-Luomalahti
// Hedelm�n abstrakti luokka. Kaikki hedelm�t periytet��n t�st� luokasta.

package tut.fi.uusiluom.PalloPeli;

import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.DisplayMetrics;

public abstract class Fruit {
	private int fruitCoord_X, fruitCoord_Y;
	private Random rndGen;
	private Context context;
	
	abstract public boolean GrabFruit(float draw_CoordX, float draw_CoordY);	//tutkitaan onko hedelm� sy�ty
	abstract public boolean IsFruitAlive();		//tutkitaan onko hedelm�n elinaika loppu
	abstract public int getPoints();
	abstract public Bitmap getBitmap();
	
	public Fruit(Context context, int screenWidth, int screenHeight){		//konstruktori
		this.context = context;
		rndGen = new Random();
		fruitCoord_X = rndGen.nextInt(screenWidth-(int)Dp2Pixel(140)) + 
    			(int)Dp2Pixel(70);
    	fruitCoord_Y = rndGen.nextInt(screenHeight-(int)Dp2Pixel(160)) + 
    			(int)Dp2Pixel(70);
	}
	
	// ************** tutkitaan osutaanko hedelm��n ***********************
	public boolean DoesTouchFruit(float FRUIT_OFFSET, float draw_CoordX, float draw_CoordY, int fruitW, int fruitH){
		if( 	( draw_CoordX > (fruitCoord_X-(fruitW/2)-Dp2Pixel(FRUIT_OFFSET)) ) && 
    			( draw_CoordX < (fruitCoord_X+(fruitW/2)+Dp2Pixel(FRUIT_OFFSET)) ) && 
    			( draw_CoordY > (fruitCoord_Y-(fruitH/2)-Dp2Pixel(FRUIT_OFFSET)) ) &&
    			( draw_CoordY < (fruitCoord_Y+(fruitH/2)+Dp2Pixel(FRUIT_OFFSET)) ) ){
			return true;
		}
			return false;
	}
	
	// ************** tutkitaan osutaanko hedelm��n ***********************
	// (porkkanan oma metodi, koska porkkana tarvitsee omat offset-m��ritykset)
	public boolean DoesTouchFruitCarrot(float FRUIT_OFFSET, float draw_CoordX, float draw_CoordY, int fruitW, int fruitH){
		if( 	( draw_CoordX > (fruitCoord_X-(fruitW/2)-Dp2Pixel(FRUIT_OFFSET+15)) ) && 
    			( draw_CoordX < (fruitCoord_X+(fruitW/2)+Dp2Pixel(FRUIT_OFFSET-15)) ) && 
    			( draw_CoordY > (fruitCoord_Y-(fruitH/2)-Dp2Pixel(FRUIT_OFFSET)) ) &&
    			( draw_CoordY < (fruitCoord_Y+(fruitH/2)+Dp2Pixel(FRUIT_OFFSET)) ) ){
			return true;
		}
			return false;
	}
	
	// ************** tutkitaan osutaanko hedelm��n ***********************
	// (vesimelonin oma metodi, koska vesimeloni tarvitsee omat offset-m��ritykset)
	public boolean DoesTouchFruitMelon(float FRUIT_OFFSET, float draw_CoordX, float draw_CoordY, int fruitW, int fruitH){
		if( 	( draw_CoordX > (fruitCoord_X-(fruitW/2)-Dp2Pixel(FRUIT_OFFSET-30)) ) && 
    			( draw_CoordX < (fruitCoord_X+(fruitW/2)+Dp2Pixel(FRUIT_OFFSET)) ) && 
    			( draw_CoordY > (fruitCoord_Y-(fruitH/2)-Dp2Pixel(FRUIT_OFFSET-30)) ) &&
    			( draw_CoordY < (fruitCoord_Y+(fruitH/2)+Dp2Pixel(FRUIT_OFFSET)) ) ){
			return true;
		}
			return false;
	}
	
    // **************** muunnetaan dp -> pixel ******************
    public float Dp2Pixel(float dp){
	  DisplayMetrics metrics = context.getResources().getDisplayMetrics();
	  float px = dp * (metrics.densityDpi/160f);
	  return px;
	}
    
    public int getCoordX(){
    	return fruitCoord_X;
    }
    
    public int getCoordY(){
    	return fruitCoord_Y;
    }
}
