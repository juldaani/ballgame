// Juho Uusi-Luomalahti 27.4.2014

// Mansikka
// perushedelm�, yksi piste

package tut.fi.uusiluom.PalloPeli;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Mansikka extends Fruit {
	
	private final int POINTS = 1;	//yhdest� mansikasta saatavat pisteet
	private final float FRUIT_OFFSET = 20;		// hedelm�n kosketusoffset (dp)
	
	private int fruitW, fruitH;
	private Bitmap fruit;
	
	public Mansikka(Context context, int screenWidth, int screenHeight){		//konstruktori
		super(context, screenWidth, screenHeight);
		fruit = BitmapFactory.decodeResource(context.getResources(),R.drawable.mansikka2); 	//hedelm�n bitmap
        this.fruitW = fruit.getWidth();
        this.fruitH = fruit.getHeight();
	}
	
	@Override
	public boolean GrabFruit(float draw_CoordX, float draw_CoordY){
		return super.DoesTouchFruit(FRUIT_OFFSET, draw_CoordX, draw_CoordY, fruitW, fruitH);
	}
	
	@Override
	public int getPoints(){
		return POINTS;
	}

	@Override
	public Bitmap getBitmap(){
		return fruit;
	}

	@Override
	public boolean IsFruitAlive() {
		return true;
	}
	
}
