package tut.fi.uusiluom.PalloPeli;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
// Juho Uusi-Luomalahti 27.4.2014

//Sieni
//Hidastaa pelaajaa

public class Sieni extends Fruit  {
	private final int POINTS = 0;	//yhdest� sienest� saatavat pisteet
	private final int FRUIT_LIFE_TIME = 300;		//hedelm�n elinaika (kierroksia onDraw -metodissa)
	private final float FRUIT_OFFSET = 20;		// hedelm�n kosketusoffset (dp)
	private final double SPEED = 0.1;
	private static final int INFLUENCE_TIME = 300;
	
	private static int laskuri = 0;
	private static boolean shroomEatenFlag = false;
	private int fruitW, fruitH;
	private Bitmap fruit;
	private boolean fruitGrabFlag;
	private int fruitAgeCounter;	//elinaikalaskuri hedelm�lle
	
	//konstruktori
	public Sieni(Context context, int screenWidth, int screenHeight){		
		super(context, screenWidth, screenHeight);
		fruit = BitmapFactory.decodeResource(context.getResources(),R.drawable.shroom); 	//hedelm�n bitmap
        this.fruitW = fruit.getWidth();
        this.fruitH = fruit.getHeight();
	}
	
	// *********************** tutkitaan, ett� vaikuttaako sy�ty hedelm� pelaajaan *******************
	// jos sieni on sy�ty, niin asetetaan pelaajalle hidastus tietyksi aikaa (laskuri)
	public static void IsShroomAffecting(){
        if( (laskuri < INFLUENCE_TIME) && shroomEatenFlag ){
        	laskuri++;
        }
        else if( (laskuri >= INFLUENCE_TIME) && shroomEatenFlag  ){
        	laskuri = 0;
        	shroomEatenFlag = false;
        	DrawCanvas.setHidastus2Default();
        }
	}
	
	public static boolean getShroomEatenFlag(){
		return shroomEatenFlag;
	}
	
	@Override
	public boolean GrabFruit(float draw_CoordX, float draw_CoordY){
		fruitGrabFlag = super.DoesTouchFruit(FRUIT_OFFSET, draw_CoordX, draw_CoordY, fruitW, fruitH);
		if(fruitGrabFlag){
			DrawCanvas.setHidastus(SPEED);	//jos hedelm� on poimittu, niin hidastetaan pelaajaa
			shroomEatenFlag = true;
		}
		return fruitGrabFlag;
	}

	@Override
	public boolean IsFruitAlive(){
		fruitAgeCounter++;
		if(fruitAgeCounter > FRUIT_LIFE_TIME){
			return false;
		}
		return true;
	}
	
	@Override
	public int getPoints(){
		return POINTS;
	}
	
	@Override
	public Bitmap getBitmap(){
		return fruit;
	}
	
}
