// Juho Uusi-Luomalahti 27.4.2014

//Sitruuna
//suurentaa kimpoamiskerrointa sein�st�

package tut.fi.uusiluom.PalloPeli;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Sitruuna extends Fruit{
		
	private final int POINTS = 2;	//yhdest� sienest� saatavat pisteet
	private final int FRUIT_LIFE_TIME = 300;		//hedelm�n elinaika (kierroksia onDraw -metodissa)
	private final float FRUIT_OFFSET = 23;		// hedelm�n kosketusoffset (dp)
	private final double BOUNCE_FACTOR = 1.0;		//asetettava kimpoamiskerroin
	private static final int INFLUENCE_TIME = 300;		//hedelm�n vaikutusaika pelaajaan
	
	private static int laskuri = 0;
	private static boolean lemonEatenFlag = false;
	private int fruitW, fruitH;
	private Bitmap fruit;
	private boolean fruitGrabFlag;
	private int fruitAgeCounter;		//elinaikalaskuri hedelm�lle
	
	//konstruktori
	public Sitruuna(Context context, int screenWidth, int screenHeight){		
		super(context, screenWidth, screenHeight);
		fruit = BitmapFactory.decodeResource(context.getResources(),R.drawable.lemon); 	//hedelm�n bitmap
	    this.fruitW = fruit.getWidth();
	    this.fruitH = fruit.getHeight();
	}
	
	// *********************** tutkitaan, ett� vaikuttaako sy�ty hedelm� pelaajaan *******************
	// jos sitruuna on sy�ty, niin asetetaan suurempi kimpoamiskerroin tietyksi aikaa (laskuri)
	public static void IsLemonAffecting(){
	    if( (laskuri < INFLUENCE_TIME) && lemonEatenFlag ){
	    	laskuri++;
	    }
	    else if( (laskuri >= INFLUENCE_TIME) && lemonEatenFlag  ){
	    	laskuri = 0;
	    	lemonEatenFlag = false;
	    	DrawCanvas.setBounceFactor2Default();
	    }
	}
	
	public static boolean getLemonEatenFlag(){
		return lemonEatenFlag;
	}
	
	@Override
	public boolean GrabFruit(float draw_CoordX, float draw_CoordY){
		fruitGrabFlag = super.DoesTouchFruit(FRUIT_OFFSET, draw_CoordX, draw_CoordY, fruitW, fruitH);
		if(fruitGrabFlag){
			DrawCanvas.setBounceFactor(BOUNCE_FACTOR);	//jos hedelm� on poimittu, niin suurennetaan kimpoamiskerrointa
			lemonEatenFlag = true;
		}
		return fruitGrabFlag;
	}
	
	@Override
	public boolean IsFruitAlive(){
		fruitAgeCounter++;
		if(fruitAgeCounter > FRUIT_LIFE_TIME){
			return false;
		}
		return true;
	}
	
	@Override
	public int getPoints(){
		return POINTS;
	}
	
	@Override
	public Bitmap getBitmap(){
		return fruit;
	}
}
