// Juho Uusi-Luomalahti 27.4.2014

// Porkkana
// Nopeuttaa pelaajaa

package tut.fi.uusiluom.PalloPeli;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Porkkana extends Fruit{
	
	private final int POINTS = 3;	//yhdest� sienest� saatavat pisteet
	private final int FRUIT_LIFE_TIME = 300;		//hedelm�n elinaika (kierroksia onDraw -metodissa)
	private final float FRUIT_OFFSET = 30;		// hedelm�n kosketusoffset (dp)
	private final double SPEED = 0.35;
	private static final int INFLUENCE_TIME = 800;		// hedelm�n vaikutusaika pelaajaan
	
	private static int laskuri = 0;
	private static boolean carrotEatenFlag = false;
	private int fruitW, fruitH;
	private Bitmap fruit;
	private boolean fruitGrabFlag;
	private int fruitAgeCounter;		//elinaikalaskuri hedelm�lle
	
	
	// ******************** konstruktori ***********************
	public Porkkana(Context context, int screenWidth, int screenHeight){		
		super(context, screenWidth, screenHeight);
		fruit = BitmapFactory.decodeResource(context.getResources(),R.drawable.carrot); 	//hedelm�n bitmap
	    this.fruitW = fruit.getWidth();
	    this.fruitH = fruit.getHeight();
	}
	
	
	// *********************** tutkitaan, ett� vaikuttaako hedelm� pelaajaan *******************
	// jos porkkana on sy�ty, niin asetetaan pelaajalle hidastus tietyksi aikaa (laskuri)
	public static void IsCarrotAffecting(){
        if( (laskuri < INFLUENCE_TIME) && carrotEatenFlag ){
        	laskuri++;
        }
        else if( (laskuri >= INFLUENCE_TIME) && carrotEatenFlag  ){
        	laskuri = 0;
        	carrotEatenFlag = false;
        	DrawCanvas.setHidastus2Default();
        }
	}
	
	public static boolean getCarrotEatenFlag(){
		return carrotEatenFlag;
	}
	
	@Override
	public boolean GrabFruit(float draw_CoordX, float draw_CoordY){
		fruitGrabFlag = super.DoesTouchFruitCarrot(FRUIT_OFFSET, draw_CoordX, draw_CoordY, fruitW, fruitH);
		if(fruitGrabFlag){
			DrawCanvas.setHidastus(SPEED);	//jos hedelm� on poimittu, niin nopeutetaan pelaajaa
			carrotEatenFlag = true;
		}
		return fruitGrabFlag;
	}
	
	@Override
	public boolean IsFruitAlive(){
		fruitAgeCounter++;
		if(fruitAgeCounter > FRUIT_LIFE_TIME){
			return false;
		}
		return true;
	}
	
	@Override
	public int getPoints(){
		return POINTS;
	}
	
	@Override
	public Bitmap getBitmap(){
		return fruit;
	}
	
	public static int getInfluenceTime(){
		return INFLUENCE_TIME;
	}

}
