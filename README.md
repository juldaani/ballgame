Mobiiliohjelmointi -kurssin harjoitustyö. Pelihahmoa ohjataan tablettia kääntelemällä (kiihtyvyysanturit).

Sovellusta on testattu vain Nexus 7 -tabletilla, joten toimiminen muilla laitteilla on epävarmaa. 